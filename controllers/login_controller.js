let userModel  = require('../models/user_model');
let jwtFactory = require('jsonwebtoken');
let properties = require('../config/properties.js')

/**
 * @RequestMapping @POST /auth/login
 */
exports.login = function(req, res){

    // get parameters
    const login = req.body.login;
    const pass  = req.body.password;

    // match the user
    userModel.findOne({username: login, password: pass}, (err, data) => {

        // found
        if(data){

            // create JWT
            const JWToken = jwtFactory.sign(
              {id: data._id, 
                lastName: data.lastName,
                firstName: data.firstName,
                email: data.email,
                login: login,
                created: data.created},
              properties.tokenKey,
              {expiresIn: 3600}
            );

            // return, not as a cookie for a generique behavior in each FRONT part (angular, mobile...)
            res.json({"token": JWToken, success: true, message: "Successfully logged in."});

        }
        
        else{
          userModel.findOne({email: login, password: pass}, (err, data) => {

            // found
            if(data){
    
                // create JWT
                const JWToken = jwtFactory.sign(
                  {id: data._id, 
                  lastName: data.lastName,
                  firstName: data.firstName,
                  email: data.email,
                  login: login,
                  created: data.created},
                  properties.tokenKey,
                  {expiresIn: 3600}
                );
    
                // return, not as a cookie for a generique behavior in each FRONT part (angular, mobile...)
                res.json({"token": JWToken, success: true, message: "Successfully logged in."});
    
            }
            
            else{
                // unfound -> reject
                res.status(400).json({success: false, message: 'Login or password unknown/incorrect.'})
            }
        });
      }
    });
}
