let app = require('express').Router();
let login_controller = require('../controllers/login_controller');
let user_controller = require('../controllers/user_controller');
/**
 * @Mapping('/rest/User/*')
 */

app.post('/login', login_controller.login);
app.post('/signup', user_controller.createuser);

module.exports = app;
