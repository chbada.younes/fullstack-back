let router = require('express').Router();
let user_controller = require('../controllers/user_controller.js')

// router.get('/', user_controller.getusers);
// router.post('/', user_controller.createuser);

router.get('/:id', user_controller.getuserByID);

router.put('/:id', user_controller.update);

router.delete('/:id', user_controller.deleteuser);

module.exports = router;
