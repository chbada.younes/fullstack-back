let mongoose = require('mongoose');
let Schema = mongoose.Schema;

module.exports = mongoose.model('User', new Schema({
    lastName:           {type: String, required: 'Name is required'},
    firstName:          {type: String, required: 'First Name is required'},
    email:              {type: String, required: 'Email is required'},
    username:           {type: String, required: 'Username is required'},
    password:           {type: String, required: 'Password is required'},
}))
